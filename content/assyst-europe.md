---
title: 'Assyst Europe'
time: '1:30'
tag: 'Presentation'
img: 'assyst-europe.jpg'
---

# Présentation des stages disponibles chez Assyst Europe

Assyst Europe est une entreprise fournissant des services IT.

Deux représentants d’Assyst sont venus à l’EPHEC afin de nous présenter les opportunités de stages pouvant être effectué au sein de leur entreprise.
Deux stages étaient disponibles cette année :
- Le premier stage proposé concernait la sécurité réseau :
   
    Assyst developpe et déploie des réseaux informatiques constitués d’un ou de plusieurs serveurs, utilisé(s) sur un seul ou plusieurs sites.
    La firme peut prendre en charge la totalité d’une infrastructure en garantissant un fonctionnement continu.
    Elle peut apporter des solutions adaptées aux besoins d’intégration et d’évolutivité des clients.

- Le second concernait la conception d’une carte IOT.

    Assyst accompagne les clients dans leurs différents projets et met à leur disposition plus de 10 ans d’expérience et de savoir faire, par ex., comme ici, dans des projets de développement.. 
