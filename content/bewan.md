---
title: 'Agoria : BeWan'
time: '3:00'
tag: 'Presentation'
img: 'bewan.jpg'
---

Le 26 mars 2019, nous nous sommes déplacés à Waterloo au siège de la société Be.wan.
Cette firme technologique s’est présentée comme pouvant fournir des services informatiques destinés aux PME et aux départements des grandes entreprises.
L’entreprise est spécialisée en logiciels, en infrastructure et intégration, consultance ainsi que des solutions de financement. 

La société est membre d’Agoria.
Comme il est noté sur leur site web,
*Agoria ouvre la voie à toutes les entreprises de Belgique que la technologie inspire et qui améliorent notre qualité de vie grâce au développement et à la mise en oeuvre d'innovations technologiques.*

*Improving quality of life*, telle est leur devise. 
