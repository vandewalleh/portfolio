---
title: 'Malta'
time: '10:00'
tag: 'English'
img: 'malte.jpg'
---

En troisième année, nous sommes allés durant 3 jours à Malte, sur l’île principale.

Malte est dépourvue de ressources naturelles, mais elle est dotée d’une position géographique stratégique au sud de l’Europe.
Elle dispose d’une économie prospère et d’un niveau de vie relativement élevé, qui repose sur le commerce extérieur,
le secteur manufacturier (composants électroniques et médicaments), les services financiers et le tourisme.
Malte a intégré l’Union européenne en 2004. 

Le secteur touristique (30 % du PIB) se montre très performant.
 
Les langues parlées à Malte sont toutes liées à son histoire.
Le maltais est la langue nationale et la langue officielle de la République de Malte.
L'anglais est la deuxième langue officielle parlée à Malte. Grâce à ce voyage et à une pratique quotidienne, j'ai
nettement amélioré mon anglais.

Sur place, nous avons appris que Malte a été l'un des premiers pays au monde à mettre en place un cadre juridique
spécialisé pour les jeux d’hasard en ligne (poker virtuel, jeux de casino). 
