---
title: 'Formation Vue.js'
time: '10:00'
tag: 'Formation'
img: 'vue.png'
---

Vue est un framework frontend libre et open-source permettant de construire des interfaces utilisateurs.
Celui-ci nous permet de séparer diverses parties de l'interface en composants individuels.
Chaque page est construite à l'aide templates composés de composants. Il est notamment possible de mettre à jour
automatiquement l'affichage d'une page suite à un changement du contenu.

L'expérience développeur est beaucoup plus agréable en utilisant un framework web tel que Vue.js comparé au système
traditionnel.
