---
title: 'Création du portfolio'
tag: 'Frontend Development'
time: '10:00'
img: 'portfolio-pagespeed.png'
---

# Technologie

Pour la réalisation de ce portfolio, voici les technologies que j'ai utilisées.

- NuxtJS
- MarkdownIt
- Vuetify
- et d'autres ...


J'utilise comme stack le 'Jamstack'. Le Jamstack n'est pas à propos de technologies spécifiques mais plutôt une nouvelle
manière de construire des sites web qui permet d'avoir de meilleurs performances, une sécurité plus élevée ainsi qu'une
meilleure expérience développeur. Pourquoi attendre la construction de pages en direct quand on peut les générer à
l'avance ? Pour ce faire, j'ai utilisé NuxtJS qui m'a permis de générer un site statique qui peut ensuite être facilement déployé.
Grâce à cela, mon site web obtient 100 % d'indice de performance sur l'outil
[PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fportfolio.simplenotes.be%2F&tab=desktop).

# Contenu

Chaque activité est dans son propre fichier markdown. Celui-ci contient des métadonnées telles que le titre ou encore
le temps comptabilisé, ainsi que le contenu principal.
Une page est générée pour chacune des activités. Les données
 
# Tableau récapitulatif

La page d'accueil contient un tableau récapitulatif des différentes activités, du temps effectué ainsi que de 
la catégorie de celle-ci. Celui-ci est automatiquement généré à partir des fichiers markdown disponible.
Également, le total des heures validées est automatiquement calculé.
