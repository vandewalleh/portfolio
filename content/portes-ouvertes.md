---
title: 'Journée portes ouvertes'
time: '2:30'
tag: 'Portes ouvertes'
img:
    - 'jpo.jpg'
    - 'jpo-odoo.jpg'
---

Pour les journées portes ouvertes de l'EPHEC 2019, Maxime Liber, Melvin Campos Casares et moi-même avons présenté 
notre projet réalisé dans le cadre du cours de développement informatique avancé orienté application.

L’objectif des journées portes ouvertes est de montrer aux étudiants intéressés de suivre le cursus de bachelier en 
Technologie de l’Informatique, quelques réalisations concrètes pouvant être opérées durant ces études.

Nous avions réalisé une application de gestion de tâches comprenant 3 modules : un module serveur ainsi que deux modules
clients, le premier en ligne de commande et l'autre faite au moyen d’une interface graphique.
Cet ensemble est structuré sous le motif architectural MVC.
À l’occasion de cette journée, nous avons pu interagir avec quelques futurs étudiants qui, majoritairement,
étaient fort intéressés par notre plateforme. En effet, ils nous ont posé une série de questions intéressantes,
quelquefois même étonnantes.

Personnellement, je pense que l’existence de ces journées portes ouvertes constitue un réel incitant pour
des futurs informaticiens à s’inscrire à notre Haute-École.
