---
title: 'Advent of Code'
tag: 'Programming Challenge'
time: '10:00'
imgs: 'advent-of-code.png'
img: 
    - 'aoc-8.png'
    - 'advent-of-code.png'
---

Avent of Code est une série de challenges de programmation qui a lieu chaque année durant
la période de l'avent soit la période qui couvre les 4 semaines précédant Noël.

Ces challenges requièrent une variété de compétences tant au niveau arithmétique que logique. 
Ce calendrier de l’avent orienté IT est constitué d’un puzzle quotidien dont la complexité augmente chaque jour.
 
Cette année, j'ai effectué ce challenge en utilisant le langage de programmation Kotlin.
Celui-ci permet de programmer de manière fonctionnelle mais aussi orientée objet.

J’ai opté pour le mode ‘compétition’ avec mon frère ainé, Bernard.
Il est informaticien et travaille dans la silicon valley.
Étant donné que le degré de difficulté varie au cours du temps, le concepteur a prévu quelques aides.
Ainsi, il m’est arrivé de rester ‘bloqué’ à un moment donné.
L’aide consistait à essayer ma solution contre un des exemples fourni dans le puzzle du jour.
En fonction de ce que j’obtenais, la réponse pouvait m’aider à corriger mon programme.
Pendant les premières deux semaines, j’ai ainsi défié quotidiennement mon grand frère.
Après cette période, j’ai préféré me consacrer pleinement à mes études.

Je trouve que ce challenge annuel de programmation me permet de s’exercer dans un contexte original.
De plus, il aide également à resserrer les liens avec mon frère.
