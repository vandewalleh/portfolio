---
title: 'FOSDEM'
time: '10:00'
tag: 'Conference'
---

FOSDEM est un événement totalement gratuit permettant aux développeurs de logiciels de se rencontrer, de partager des idées et de collaborer.
Chaque année, plusieurs milliers de développeurs de logiciels libres et open source du monde entier
se réunissent lors de cet événement qui a lieu à Bruxelles.

J’y ai été une première fois en février 2019. Ayant trouvé l'expérience très intéressante, j’y suis retourné en 2020.

En effet, en plus des nombreux exposés, des stands peuvent être utilisés pour partager des informations,
des logiciels de démonstration ou même vendre des marchandises, donner des cadeaux et, surtout, pour les exposants,
se présenter aux nombreux visiteurs.

### Samedi 2 février 2019
- 11h - 11h50 : FLOSS, the Internet and the Future
- 12h30 - 13h : Onion Adventures - How to use onion services and the Tor network in your web endeavors
- 14h - 14h50 : Matrix in the French State - What happens when a government adopts open source & open standards for all its internal communication?
- 15h - 15h50 : DNS over HTTPS - the good, the bad and the ugly (why, how, when and who gets to control how names are resolved)
- 16h20 - 16h35 : Open Software deserves 

### Dimanche 3 février 2019

- 10h15 - 10h45 : Tooling for IntelliJ Platform Plugins
- 11h - 11h50 : The Current and Future Tor Project
- 13h - 13h50 : Open Source at DuckDuckGo - Raising the Standard of Trust Online
- 16h - 16h50 : The Cloud is Just Another Sun
- 17h - 17h50 : Fifty years of Unix and Linux advances
- 17h55 - 18h : Closing FOSDEM 2019

### Dimanche 2 février 2020

- 10h - 10h50 : Is the Open door closing?
- 11h - 11h50 : The core values of software freedom
- 14h30 - 15h : The Path to Peer-to-Peer Matrix
- 15h - 15h30 : Building a Web App that doesn't Trust the Server
- 16h - 16h50 : FOSSH - 2000 to 2020 and beyond!
