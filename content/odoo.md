---
title: 'Odoo'
time: '1:30'
tag: 'Presentation'
img: 'jpo-odoo.jpg'
---

Deux représentants de la firme brabançonne Odoo sont venus présenter leur société et la gamme de produits en novembre 2018.


OpenERP, société belge créée en 2005, a changé de nom en mai 2014 pour prendre celui d’Odoo.
Leur mission est de fournir une gamme d'applications professionnelles faciles à utiliser, qui forment une suite complète d'outils et qui répondant aux besoins des entreprises. 
Odoo a développé quelque 30 applications.
Le prologiciel principal est une suite d'applications professionnelles et open source couvrant les besoins des entreprises
: 
- CRM
- eCommerce
- comptabilité
- inventaire
- point de vente
- gestion de projets
- etc

Il compte plusieurs millions d’utilisateurs.

Frequemment, les firmes ont sélectionné Odoo pour la facilité de mise en œuvre de ses modules standards qui permettent la gestion de toutes les activités de l’organisation er ceci, semble t’il à un prix très compétitif.
La flexibilité d’Odoo due à sa grande modularité lui confère aussi un avantage non négligeable.
Odoo permet aussi de connecter l’application à n’importe quel portail web ou logiciel grâce à ses API prêts à l’emploi.



