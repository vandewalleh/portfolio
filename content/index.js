export default [
    'advent-of-code',
    'assyst-europe',
    'azure',
    'bewan',
    'formation-vue',
    'fosdem',
    'malta',
    'odoo',
    'portes-ouvertes',
    'portfolio',
    'search'
]
